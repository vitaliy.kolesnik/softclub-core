package by.softclub.core;

import by.softclub.core.annotation.SimpleComponent;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

public class SoftclubApplicationContext {

    private static Map<String, Object> beans = new HashMap<>();

    public static void main(String[] args) throws IOException, URISyntaxException {
        packageScan();
        beans.forEach((k, v) -> System.out.printf("%s -> %s\n", k, v.toString()));
    }

    private static void packageScan() throws IOException, URISyntaxException {
        String packageName = SoftclubApplicationContext.class
                .getPackage().getName();
        String path = packageName.replace(".", "/");
        // Получим class loader
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        Enumeration<URL> resources = loader.getResources(path);

        List<File> dirs = new ArrayList<>();
        for (URL u : Collections.list(resources)) {
            dirs.add(new File(u.toURI().getPath()));
        }

        beans = dirs.stream()
                .flatMap((File dir) -> findClasses(dir, packageName).stream())
                .filter(cls -> cls.isAnnotationPresent(SimpleComponent.class))
                .collect(Collectors.toMap(
                        SoftclubApplicationContext::getClassNameOrAnnotation,
                        SoftclubApplicationContext::createObject
                        ));

    }

    private static String getClassNameOrAnnotation(Class<?> cls) {
        String annotation = ((SimpleComponent) cls.getAnnotation(SimpleComponent.class)).name();
        if (!annotation.isEmpty()) {
            return annotation;
        } else {
            String className = cls.getSimpleName();
            return className.toLowerCase().charAt(0) + className.substring(1);
        }
    }

    // получение списка классов в пакете
    private static List<Class<?>> findClasses(File dir, String packageName) {
        if (!dir.exists()) {
            return Collections.emptyList();
        }
        File[] files = dir.listFiles();
        if (files == null) {
            return Collections.emptyList();
        }
        List<Class<?>> classes = new ArrayList<>();

        for (File file : files) {
            if (file.isDirectory()) {
                classes.addAll(findClasses(file, packageName + "." + file.getName()));
            } else if (file.getName().endsWith(".class")) {
                try {
                    classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
                } catch (ClassNotFoundException e) {
                    //
                }
            }
        }
        return classes;
    }

    private static Object createObject(Class<?> cls) {
        Object obj = null;
        try {
            obj = cls.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return obj;
    }

}


